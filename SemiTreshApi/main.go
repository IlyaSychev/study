package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/username/reponame/utils"
)

const (
	apiPrefix string = "/api/v1"
)

var (
	port                    string
	bookResourcePrefix      string = apiPrefix + "/book"
	manyBooksResourcePrefix string = apiPrefix + "/books"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
	port = os.Getenv("app_port")
}

func main() {
	log.Println("Strating server")
	router := mux.NewRouter()

	utils.BuildBookResourcePrefix(router, bookResourcePrefix)
	utils.BuildManyBookResourcePrefix(router, manyBooksResourcePrefix)

	log.Println("Strating server")
	log.Fatal(http.ListenAndServe(":"+port, router))
}
