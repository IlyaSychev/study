package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/username/reponame/models"
)

func initHeaders(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
}

func GetAllBooks(w http.ResponseWriter, r *http.Request) {
	initHeaders(w)
	log.Println("Get infos about all books")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(models.DB)
}
