module github.com/username/reponame

go 1.21.1

require (
	github.com/gorilla/mux v1.8.1
	github.com/joho/godotenv v1.5.1
)
